import React, {Component} from "react";
import PropTypes from 'prop-types';

export const IMAGE_KEY = 'infinite_images';

export default class Image extends Component {

    constructor(props) {
        super(props);
        this.state = {
            src: props.src,
            selected: props.selected || false,
            loaded: false
        };
    }

    onImageLoaded() {
        this.setState({
            loaded: true
        });
    }

    onSelect() {
        this.setState({
            selected: !this.state.selected
        });
        this._saveImage();
    }

    _saveImage() {
        setTimeout(() => {
            const images = JSON.parse(localStorage.getItem(IMAGE_KEY)) || [];
            if (this.state.selected)
                images.push(this.state.src);
            else {
                const index = images.indexOf(this.state.src);
                if (index > -1) images.splice(index, 1);
            }
            localStorage.setItem(IMAGE_KEY, JSON.stringify(images));
        });
    }

    render() {
        const {
            src, loaded, selected
        } = this.state;
        const classSelected = selected ? 'selected' : null;
        const classLoaded = loaded ? 'loaded' : null;

        return (
            <div onClick={this.onSelect.bind(this)} className={classSelected}>
                <img
                    className={classLoaded}
                    src={src}
                    alt={src}
                    onLoad={this.onImageLoaded.bind(this)}
                />
                {loaded && <span>☆</span>}
            </div>
        );
    }
}

Image.propTypes = {
    src: PropTypes.string.isRequired,
    selected: PropTypes.bool
};