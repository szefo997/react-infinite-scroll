import React, {Component, Fragment} from "react";
import axios from "axios";

import './_infinite-images.scss';
import Image, {IMAGE_KEY} from "./Image";

export default class InfiniteImages extends Component {

    constructor(props) {
        super(props);
        this.state = {
            error: false,
            hasMore: true,
            isLoading: false,
            pending: false,
            images: [],
        };
        this._bindOnScroll();

        setTimeout(() => {
            this._loadImages();
        }, 25);
    }

    static _isScrolledToBottom() {
        const scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
        const scrollHeight = (document.documentElement && document.documentElement.scrollHeight) || document.body.scrollHeight;
        const clientHeight = document.documentElement.clientHeight || window.innerHeight;
        return Math.ceil(scrollTop + clientHeight) >= scrollHeight;
    }

    static _retrieveImagesFromLocalStorage() {
        return JSON.parse(localStorage.getItem(IMAGE_KEY)) || [];
    }

    _bindOnScroll() {
        window.onscroll = () => {
            const {
                _loadImages,
                state: {
                    error,
                    isLoading,
                    hasMore,
                },
            } = this;

            if (error || isLoading || !hasMore) return;
            if (InfiniteImages._isScrolledToBottom()) _loadImages.call(this);
        };
    }

    async _loadImages() {
        this.setState({isLoading: true});
        const results = await axios.get('http://shibe.online/api/shibes?count=16')
            .catch((err) => {
                this.setState({
                    error: err.message,
                    isLoading: false,
                });
            });

        this.setState({
            hasMore: true,
            isLoading: false,
            images: this.state.images.concat(this._mapToObjectAndCheckInLocalStorage(results))
        });
    }

    _mapToObjectAndCheckInLocalStorage(results) {
        const arr = [];
        results.data.forEach(v => {
            let isSelected = false;
            const selectedImages = InfiniteImages._retrieveImagesFromLocalStorage();

            for (const s of selectedImages)
                if (v === s) {
                    isSelected = true;
                    break;
                }

            arr.push({
                url: v,
                isSelected
            });
        });
        return arr;
    }

    onToggleSelected() {
        if (this.state.hasMore === false) {
            this.setState({
                hasMore: true,
                isLoading: false,
                images: []
            });
            this._loadImages();
            return;
        }

        const images = [];
        const fromStorage = InfiniteImages._retrieveImagesFromLocalStorage();

        fromStorage.forEach(v => {
            images.push({
                url: v,
                isSelected: true
            });
        });

        this.setState({
            hasMore: false,
            isLoading: false,
            images
        });
    }

    render() {
        const {
            error,
            hasMore,
            isLoading,
            images,
        } = this.state;

        return (
            <div>
                <header>
                    <div className="navbar navbar-dark bg-dark box-shadow">
                        <div className="d-flex justify-content-between">
                            <a href="#" className="navbar-brand d-flex align-items-center">
                                <strong>Infinite images</strong>
                            </a>
                            <button type="button" onClick={this.onToggleSelected.bind(this)}
                                    className="btn btn-primary pull-xs-right">
                                {hasMore && <span>Show selected</span>}
                                {!hasMore && <span>Show All</span>}
                            </button>
                        </div>
                    </div>
                </header>

                <main>
                    <div className={'container'}>
                        <div className="row">
                            {images.map((image, index) => (
                                <Fragment key={index}>
                                    <div className={'wrap-image col-lg-3 col-md-4 col-sm-6'}>
                                        <Image
                                            src={image.url}
                                            selected={image.isSelected}
                                            key={image.url}
                                        />
                                    </div>
                                </Fragment>
                            ))}
                        </div>

                        <hr/>
                        {error &&
                        <div style={{color: '#900'}}>
                            {error}
                        </div>
                        }
                        {isLoading &&
                        <div>Loading...</div>
                        }
                    </div>
                </main>
            </div>


        );
    }
}

