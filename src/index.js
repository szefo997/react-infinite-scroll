import React from 'react';
import ReactDOM from 'react-dom';
import 'normalize.css';
import './styles/app.scss';
import InfiniteImages from "./components/InfiniteImages";

ReactDOM.render(< InfiniteImages/>, document.getElementById('app'));